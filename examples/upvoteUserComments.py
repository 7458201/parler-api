import sys
import time
import json
from parler import Parler

mst = ''
jst = ''

if len(sys.argv) < 2:
    print(f'Usage: {sys.argv[0]} username')
    sys.exit()

username = sys.argv[1]

client = Parler(mst, jst)

# get first page of the user's comments
results = client.getCommentsOfUser(username)

for comment in results['comments']:
    # Upvote each comment
    client.commentVote(comment['_id'], up = True) # change up = False to downvote >:)
    print(f'Upvoted Comment: {comment["_id"]}')
    time.sleep(0.3)
