# Parler Python API
Parler Social Media Python API

### Install
```sh
$ pip install parler
```

### Authentication
After you login to Parler.com you can get the `MST` and `JST` tokens:

`Open DevTools -> Storage -> Cookies`

### Usage
```python
from parler import Parler

mst = 'XXX' # see authentication section
jst = 'YYY'

client = Parler(mst, jst)

print(client.getFeed());
```

### Methods
Note: Currently their API is ignoring the `limit` parameter for any API call and always returning 10.

Paging: Uses the `startKey` parameter which is a timestamp. This comes from the `next` and `last` values of a request.

##### post(body, links = [], sensitive = False, state = 4)
Creates a post

##### deletePost(postId)
Deletes a post or echo

##### echo(postId, links = [], sesitive = False, state = 4)
echos a post

##### commentEcho(postId, body = '', links = [], sensitive = False, state = 4)
echos a post with a comment

##### comment(postId, body = '', links = [], sensitive = False)
Posts a comment to the PostId

##### deleteComment(postId)
Deletes a comment on a post

##### deletePostUpvote(postId)
Deletes an upvote for a post

##### commentVote(postId, up = True)
Votes a comment on a post. Use up = True for an upvote or up = False for downvote.

##### deleteCommentVote(postId)
Deletes a 'vote' for a comment given the postId

##### profile(username = False)
Gets user profile information. If username is not defined, return the logged in user's information.

##### follow(username)
Follow the username

##### unfollow(username)
Unfollows a user by their username

##### block(username)
Blocks a username

##### unblock(userId)
Unblocks a user by their userId

##### settings()
Gets the settings for the logged in user

##### updateProfile(bio = False, location = False, name = False, title = False, username = False, accountColor = False)
Updates the logged in user's profile information

##### getPostsOfUserId(userId, limit = 10, startKey = False)
Gets the posts for the userId with a specified limit

##### getLikesOfUserId(userId, limit = 10, startKey = False)
Gets the liked posts by a userId

##### getCommentsOfUser(username, limit = 10, startKey = False)
Gets the comments of the given username

##### getFollowingOfUserId(userId, limit = 10, startKey = False)
Gets the following list of the userId

##### getFollowersOfUserId(userId, limit = 10, startKey = False)
Gets the list of user who the user is following

##### userSearch(username = '')
Gets a list of user details given the username, if no username is defined, return a list of shit users they promote.

##### hashtagSearch(hashtag = '')
Gets a list of hashtags and their associated counts given a hashtag. If no hashtag is defined, it returns what is trending.

##### getFeed(limit = 10, startKey = False, hideEchos = False, onlySubscribed = False)
Returns the 'feed' of the currently logged in user.

##### getNotifications(limit = 10, startKey = False)
Gets the logged in user's notifications

##### deleteAllNotifcations()
Deletes all the notifications for the logged in user

#### More
```sh
$ python
Python 3.7.3 (default, May 31 2019, XX:XX:XX)
[GCC 5.4.0 2016XXXX] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> import parler
>>> help(parler)
```

#### TODO
mute/unmute
conversations? ehh
moderation? ehhhhhh